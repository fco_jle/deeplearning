import numpy as np


def sigmoid(z):
    s = 1 / (1 + np.exp(-z))
    return s


def initialize_with_zeros(dim):
    w = np.zeros((dim, 1))
    b = 0
    assert (w.shape == (dim, 1))
    assert (isinstance(b, float) or isinstance(b, int))
    return w, b


def propagate(w, b, x, y):
    m = x.shape[1]
    a = sigmoid(np.dot(w.T, x) + b)  
    cost = -1 / m * np.sum(y * np.log(a) + (1 - y) * np.log(1 - a))
    dw = 1 / m * (np.dot(x, (a - y).T))
    db = 1 / m * np.sum(a - y)
    assert (dw.shape == w.shape)
    assert (db.dtype == float)
    cost = np.squeeze(cost)
    assert (cost.shape == ())
    grads = {"dw": dw,
             "db": db}
    return grads, cost


def optimize(w, b, x, y, num_iterations, learning_rate, print_cost=False):
    costs = []
    dw = 0
    db = 0
    for i in range(num_iterations):
        grads, cost = propagate(w, b, x, y)
        dw = grads["dw"]
        db = grads["db"]
        w = w - learning_rate * dw
        b = b - learning_rate * db
        if i % 100 == 0:
            costs.append(cost)
        if print_cost and i % 100 == 0:
            print("Cost after iteration %i: %f" % (i, cost))

    params = {"w": w, "b": b}
    grads = {"dw": dw, "db": db}
    return params, grads, costs


def predict(w, b, x):
    m = x.shape[1]
    y_prediction = np.zeros((1, m))
    w = w.reshape(x.shape[0], 1)
    a = sigmoid(np.dot(w.T, x) + b)
    for i in range(a.shape[1]):
        if a[0, i] >= 0.5:
            y_prediction[0, i] = 1
        else:
            y_prediction[0, i] = 0
    assert (y_prediction.shape == (1, m))
    return y_prediction


def model(x_train, y_train, x_test, y_test, num_iterations=2000, learning_rate=0.5, print_cost=False):
    w, b = initialize_with_zeros(x_train.shape[0])
    parameters, grads, costs = optimize(w, b, x_train, y_train, num_iterations, learning_rate, print_cost)
    w = parameters["w"]
    b = parameters["b"]
    y_prediction_test = predict(w, b, x_test)
    y_prediction_train = predict(w, b, x_train)
    print("train accuracy: {} %".format(100 - np.mean(np.abs(y_prediction_train - y_train)) * 100))
    print("test accuracy: {} %".format(100 - np.mean(np.abs(y_prediction_test - y_test)) * 100))
    d = {"costs": costs,
         "y_prediction_test": y_prediction_test,
         "y_prediction_train": y_prediction_train,
         "w": w,
         "b": b,
         "learning_rate": learning_rate,
         "num_iterations": num_iterations}
    return d
