import numpy as np


def sigmoid(z):
    a = 1 / (1 + np.exp(-z))
    cache = z
    return a, cache


def relu(z):
    a = np.maximum(0, z)
    assert (a.shape == z.shape)
    cache = z
    return a, cache


def relu_backward(da, cache):
    z = cache
    dz = np.array(da, copy=True)
    dz[z <= 0] = 0
    assert (dz.shape == z.shape)
    return dz


def sigmoid_backward(da, cache):
    z = cache
    s = 1 / (1 + np.exp(-z))
    dz = da * s * (1 - s)
    assert (dz.shape == z.shape)
    return dz


def initialize_parameters(n_x, n_h, n_y):
    np.random.seed(1)
    w1 = np.random.randn(n_h, n_x) * 0.01
    b1 = np.zeros((n_h, 1))
    w2 = np.random.randn(n_y, n_h) * 0.01
    b2 = np.zeros((n_y, 1))
    assert (w1.shape == (n_h, n_x))
    assert (b1.shape == (n_h, 1))
    assert (w2.shape == (n_y, n_h))
    assert (b2.shape == (n_y, 1))
    parameters = {"w1": w1,
                  "b1": b1,
                  "w2": w2,
                  "b2": b2}
    return parameters


def initialize_parameters_deep(layer_dims):
    np.random.seed(1)
    parameters = {}
    l_dim = len(layer_dims)
    for l in range(1, l_dim):
        parameters['w' + str(l)] = np.random.randn(layer_dims[l], layer_dims[l - 1]) / np.sqrt(
            layer_dims[l - 1])
        parameters['b' + str(l)] = np.zeros((layer_dims[l], 1))
        assert (parameters['w' + str(l)].shape == (layer_dims[l], layer_dims[l - 1]))
        assert (parameters['b' + str(l)].shape == (layer_dims[l], 1))
    return parameters


def linear_forward(a, w, b):
    z = np.dot(w, a) + b
    assert (z.shape == (w.shape[0], a.shape[1]))
    cache = (a, w, b)
    return z, cache


def linear_activation_forward(a_prev, w, b, activation):
    if activation == "sigmoid":
        z, linear_cache = linear_forward(a_prev, w, b)
        a, activation_cache = sigmoid(z)
    elif activation == "relu":
        z, linear_cache = linear_forward(a_prev, w, b)
        a, activation_cache = relu(z)
    else:
        raise Exception('activation not supported')
    assert (a.shape == (w.shape[0], a_prev.shape[1]))
    cache = (linear_cache, activation_cache)
    return a, cache


def l_model_forward(x, parameters):
    caches = []
    a = x
    l_dim = len(parameters) // 2

    for l in range(1, l_dim):
        a_prev = a
        a, cache = linear_activation_forward(a_prev, parameters['w%s' % l], parameters['b%s' % l], 'relu')
        caches.append(cache)

    al_dim, cache = linear_activation_forward(a, parameters['w%s' % l_dim], parameters['b%s' % l_dim], 'sigmoid')
    caches.append(cache)
    assert (al_dim.shape == (1, x.shape[1]))
    return al_dim, caches


def compute_cost(al_dim, y):
    m = y.shape[1]
    cost = -1 / m * np.sum(y * np.log(al_dim) + (1 - y) * np.log(1 - al_dim))
    cost = np.squeeze(cost)
    assert (cost.shape == ())
    return cost


def linear_backward(dz, cache):
    a_prev, w, b = cache
    m = a_prev.shape[1]

    dw = 1 / m * np.dot(dz, a_prev.T)
    db = 1 / m * np.sum(dz, axis=1, keepdims=True)
    da_prev = np.dot(w.T, dz)

    assert (da_prev.shape == a_prev.shape)
    assert (dw.shape == w.shape)
    assert (db.shape == b.shape)

    return da_prev, dw, db


def linear_activation_backward(da, cache, activation):
    linear_cache, activation_cache = cache

    if activation == "relu":
        dz = relu_backward(da, activation_cache)
        da_prev, dw, db = linear_backward(dz, linear_cache)
    elif activation == "sigmoid":
        dz = sigmoid_backward(da, activation_cache)
        da_prev, dw, db = linear_backward(dz, linear_cache)
    else:
        raise Exception('activation not supported')
    return da_prev, dw, db


def l_model_backward(al_dim, y, caches):
    grads = {}
    l_dim = len(caches)
    y = y.reshape(al_dim.shape)
    dal_dim = - (np.divide(y, al_dim) - np.divide(1 - y, 1 - al_dim))

    current_cache = caches[l_dim - 1]
    grads["da" + str(l_dim - 1)], grads["dw" + str(l_dim)], grads["db" + str(l_dim)] = \
        linear_activation_backward(dal_dim, current_cache, 'sigmoid')
    for l in reversed(range(l_dim - 1)):
        current_cache = caches[l]
        da_prev_temp, dw_temp, db_temp = linear_activation_backward(grads["da" + str(l + 1)], current_cache, 'relu')
        grads["da" + str(l)] = da_prev_temp
        grads["dw" + str(l + 1)] = dw_temp
        grads["db" + str(l + 1)] = db_temp

    return grads


def update_parameters(parameters, grads, learning_rate):
    l_dim = len(parameters) // 2

    for l in range(l_dim):
        parameters["w" + str(l + 1)] = parameters["w" + str(l + 1)] - learning_rate * grads["dw" + str(l + 1)]
        parameters["b" + str(l + 1)] = parameters["b" + str(l + 1)] - learning_rate * grads["db" + str(l + 1)]

    return parameters


def predict(x, y, parameters):
    m = x.shape[1]
    p = np.zeros((1, m))
    probas, caches = l_model_forward(x, parameters)
    for i in range(0, probas.shape[1]):
        if probas[0, i] > 0.5:
            p[0, i] = 1
        else:
            p[0, i] = 0
    print("Accuracy: " + str(np.round(np.sum((p == y) / m), 3)))
    return p
